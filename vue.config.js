module.exports = {
  lintOnSave: false,
    // css 配置
      css: {
        loaderOptions: {
          postcss: {
            plugins: [
            require("postcss-pxtorem")({
              rootValue: 100,
              propList: ['*']
            })
        ]
          }
        }
      }
    }